package pl.edu.pwr.hello;

import org.junit.*;
import org.junit.Before;
import org.junit.Test;


import org.hamcrest.*;
import org.hamcrest.core.*;
import org.hamcrest.internal.*;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import javax.print.attribute.Size2DSyntax;

public class MainHoroskopTest {
	
	Horoskop horoskopTest = new Horoskop();
	
	@Before
	public void initialize(){
		horoskopTest.setImie("Emil");
		horoskopTest.setNazwisko("Klepacz");
	}
	
	@Test
	public void CzyDobryIndexZdrowie(){
		assertThat(11, is(equalTo(horoskopTest.zwrocIndexZdrowie())));
	}

	@Test
	public void CzyDobraWrozbaZdrowie() {
		//"Emil Klepacz" zawiera 11 liter wiec 
		//wrozba powinna byc ta o indexie 11 (bo 11 mod 12 = 11)
		assertThat("Wprowadź do swego życia więcej radości." , is(equalTo(horoskopTest.wrozbaZdrowie())));
	}
	
	@Test
	public void CzyIndexMiloscJestIntemINieJestNullem(){
		int indexMilosc = horoskopTest.zwrocIndexMilosc();
		assertThat(indexMilosc, is(allOf(notNullValue(), instanceOf(Integer.class))) );
	}
	
	@Test
	public void CzyindexPracaJestMiedzyZeroAJedenascie(){
		Assert.assertTrue(0 <= horoskopTest.zwrocindexPraca() && horoskopTest.zwrocindexPraca() <= 11);
	}
	
	@Test
	public void CzyTablicePrzepowiedniMajaPoDwanasciePrzepowiedni(){
		Assert.assertTrue(horoskopTest.getPrzepowiednie().MILOSC.length == 12);
		Assert.assertTrue(horoskopTest.getPrzepowiednie().PRACA.length == 12);
		Assert.assertTrue(horoskopTest.getPrzepowiednie().ZDROWIE.length == 12);
	}

}
